import { reactive, ref } from 'vue'
import { http } from '@/utils/interceptors'
import axios from 'axios'

const productPayload = reactive({
  keyword: '',
  price: '0,5000000',
  page: 1,
  limit: 10,
  order: 'product_name'
})

const products = ref<any>(undefined)
const product = ref<any>(undefined)


const useProducts = () => {
  const getProducts = async () => {
    const response = await http
      .get('products', {
        params: { ...productPayload }
      })
      .then((response: any) => (response))
      .catch(async () => {
        return await axios.get('/mockup/product-list.json')
          .then((response: any) => {
            products.value = response
          })
      })

    return response
  }

  return {
    getProducts
  }
}

export {
  productPayload,
  products,
  product,
  useProducts
}
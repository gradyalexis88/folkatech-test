import { reactive, ref } from 'vue'
import { http } from '@/utils/interceptors'
import type { LoginPayload } from '../types/auth'

const registerData = reactive({
  firstName: '',
  lastName: '',
  email: '',
  phone: '',
  password: '',
  confirmPassword: ''
})

const registerValidation = ref<any>(undefined)

const useAuth = () => {
  const login = async (payload: LoginPayload) => {
    const response = await http
      .post('login', payload)
      .then((response: any) => {
        const user = response.data.data
        localStorage.setItem('user', JSON.stringify(user))
        localStorage.setItem('token', JSON.stringify(user.token))

        return response
      })
      .catch((error: any) => (error.response))

    return response
  }

  const register = async () => {
    const payload = {
      ...registerData,
      name: `${registerData.firstName} ${registerData.lastName}`
    }
    const response = await http
      .post('register', payload)
      .then((response: any) => (response))
      .catch((error: any) => (error.response))

    return response
  }

  const logout = () => {
    localStorage.removeItem('user')
    localStorage.removeItem('token')
  }

  return {
    login,
    register,
    logout
  }
}

export {
  useAuth,
  registerData,
  registerValidation
}
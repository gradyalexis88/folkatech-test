import axios from 'axios'

const token = localStorage.getItem('token') || ''

export const http = axios.create({
  baseURL: 'https://techtest.folkatech.com/api/',
	headers: {
		'Access-Control-Allow-Origin': '*',
		'Access-Control-Allow-Credentials': true,
		'Authorization': `${token.replace(/\"/g, '')}`
	}
})

http.interceptors.request.use((config) => {
  config.params = config.params || {}
  // config.params.token = token

  return config
})

http.interceptors.response.use(
	(response) => {
		if (response.status >= 500) {
			return Promise.reject(response)
		} else return response
	},
	(error) => {
		const status = error.response.status
		const message = 'Error request'
		console.error(message, status)
		return Promise.reject(error)
	}
)
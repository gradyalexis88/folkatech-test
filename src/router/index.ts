import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/auth/Login.vue'
import Register from '../views/auth/Register.vue'
import ShoppingList from '../views/shopping/ProductList.vue'
import ShoppingDetail from '../views/shopping/ProductDetail.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/shopping',
      name: 'shopping',
      component: ShoppingList
    },
    {
      path: '/shopping/detail',
      name: 'shoppingDetail',
      component: ShoppingDetail
    }
  ]
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('token');

  if (authRequired && !loggedIn) {
    next({ name: 'login' });
  } else {
    next();
  }
});

export default router
